<?php

namespace KITT3N\Pimcore\BaseUserAndRoleBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreBaseUserAndRoleBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorebaseuserandrole/js/pimcore/startup.js'
        ];
    }
}