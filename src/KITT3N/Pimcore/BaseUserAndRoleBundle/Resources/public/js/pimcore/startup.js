pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreBaseUserAndRoleBundle");

pimcore.plugin.Kitt3nPimcoreBaseUserAndRoleBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreBaseUserAndRoleBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreBaseUserAndRoleBundle ready!");
    }
});

var Kitt3nPimcoreBaseUserAndRoleBundlePlugin = new pimcore.plugin.Kitt3nPimcoreBaseUserAndRoleBundle();
